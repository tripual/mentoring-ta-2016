package com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions;

public class ReadWriteException extends RuntimeException {

    public ReadWriteException(String message, Throwable cause) {
        super(message, cause);
    }
}
