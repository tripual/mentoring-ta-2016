package com.epam.cdp.java_testing.aliaksei_tripuz.task3;

public class Calculator {

    private int value1;
    private int value2;

    public Calculator(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public int sum() {
        return this.value1 + this.value2;
    }

    public int abs() {
        return this.value1 - this.value2;

    }

    public int mult() {
        return this.value1 * this.value2;

    }

    public int div() {
        return this.value1 / this.value2;

    }


}

