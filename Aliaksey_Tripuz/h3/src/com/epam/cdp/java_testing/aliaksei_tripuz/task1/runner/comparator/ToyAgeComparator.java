package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.Comparator;

public class ToyAgeComparator implements Comparator<Toy> {

    @Override
    public int compare(Toy o1, Toy o2) {
        return o1.getToyAge() - o2.getToyAge();

    }

}

