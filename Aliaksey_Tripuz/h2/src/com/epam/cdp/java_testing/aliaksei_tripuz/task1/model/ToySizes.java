package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;


public interface ToySizes {

    int LITTLE_SIZE = 2;
    int MIDDLE_SIZE = 4;
    int BIG_SIZE = 6;
}
