package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

public class FindConditionByAge implements FindCondition {

    private int fromRange;
    private int toRange;


    public FindConditionByAge(int fromRange, int toRange) {
        this.fromRange = fromRange;
        this.toRange = toRange;
    }

    @Override
    public boolean apply(Toy toy) {
        int size = toy.getToyAge();
        return size >= fromRange && size <= toRange;
    }


}