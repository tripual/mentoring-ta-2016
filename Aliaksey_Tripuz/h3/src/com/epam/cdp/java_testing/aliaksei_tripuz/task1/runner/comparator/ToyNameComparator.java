package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.Comparator;

public class ToyNameComparator implements Comparator<Toy> {

    @Override
    public int compare(Toy o1, Toy o2) {
        String name1 = o1.getToyName();
        String name2 = o2.getToyName();
        return name1.compareTo(name2);

    }

}

