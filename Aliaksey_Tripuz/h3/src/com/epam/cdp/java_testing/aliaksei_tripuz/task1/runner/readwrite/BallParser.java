package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateToyFieldsException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Ball;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.List;

import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.END_TOKEN;
import static com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.IStartEndToken.START_TOKEN;


public class BallParser extends ToyParser {

    private String ballRubber;
    private String className = Ball.class.getName();

    @Override
    public Toy parseToy(List<String> toyObject) {
        for (String toyToken : toyObject) {
            if (toyObject.get(0).equals(START_TOKEN.trim() + "=" + className.trim())) {
                setToyFields(toyToken);
                if (toyToken.startsWith("ballRubber")) {
                    ballRubber = toyToken.substring(toyToken.lastIndexOf("=") + 1);
                }
                if (toyToken.equals(END_TOKEN.trim() + "=" + className.trim())) {
                    Ball ball = new Ball(toyName, toyAge, toyCost, toySize, ballRubber);
                    if (validateToy()) {
                        return ball;
                    } else {
                        throw new ValidateToyFieldsException(
                                "Toy validation failed due to some toy field(s) illegal value.\n" +
                                        "Not added toy is: " + ball);
                    }
                }
            }
        }
        return null;
    }

    @Override
    boolean validateToy() {
        return toyName != null && toyCost != 0 && ballRubber != null;
    }

}
