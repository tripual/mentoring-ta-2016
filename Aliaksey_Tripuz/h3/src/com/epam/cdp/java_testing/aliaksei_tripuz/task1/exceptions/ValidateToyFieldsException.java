package com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions;

public class ValidateToyFieldsException extends RuntimeException {

    public ValidateToyFieldsException(String message) {
        super(message);
    }

}
