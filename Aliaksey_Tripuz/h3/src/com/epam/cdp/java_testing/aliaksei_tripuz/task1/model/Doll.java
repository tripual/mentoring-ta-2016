package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

public class Doll extends Toy {

    private String hair;

    public Doll(String toyName, int toyAge, double toyCost, int toySize, String hair) {
        super(toyName, toyAge, toyCost, toySize);
        this.hair = hair;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    @Override
    public void play() {
        super.play();
        setHair("Dress " + getHair());

    }

    @Override
    public String toString() {
        return super.getInfo() + ", Hair: " + getHair();
    }
}
