package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ReadWriteException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateMaxTotalToysCostException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateMaxTotalToysQuantityException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateToyFieldsException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.*;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator.ToyAgeComparator;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator.ToyCostComparator;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator.ToyNameComparator;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.findcondition.FindConditionByAge;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.findcondition.FindConditionByCostRange;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.findcondition.FindConditionByName;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.findcondition.FindConditionByToySize;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.ToysReader;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite.ToysWriter;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.toyfinder.ToyFinderByAge;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.toyfinder.ToyFinderByCost;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.toyfinder.ToyFinderByName;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.toyfinder.ToyFinderBySize;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;

public class Runner {

    public static void main(String[] args) {

        PlayRoom room = null;

        try {
            room = new PlayRoom(100, 10);

        } catch (ValidateMaxTotalToysCostException | ValidateMaxTotalToysQuantityException e) {
            e.printStackTrace();
            return;
        }

        room.addToy(new Store().buyCar(ToySize.LITTLE));
        room.addToy(new Store().buyCar(ToySize.MIDDLE));
        room.addToy(new Store().buyCar(ToySize.BIG));

        room.addToy(new Store().buyBall(ToySize.LITTLE));
        room.addToy(new Store().buyBall(ToySize.MIDDLE));
        room.addToy(new Store().buyBall(ToySize.BIG));

        room.addToy(new Store().buyDoll(ToySize.LITTLE));
        room.addToy(new Store().buyDoll(ToySize.MIDDLE));
        room.addToy(new Store().buyDoll(ToySize.BIG));

        room.addToy(new Store().buyBrick(ToySize.LITTLE));
        room.addToy(new Store().buyBrick(ToySize.MIDDLE));
        room.addToy(new Store().buyBrick(ToySize.BIG));

        for (String notAddedToy : room.getNotAddedToys()) {
            System.out.println(notAddedToy);
        }

        String file = "toysList.txt";

        ToysWriter.writeToysToFile(room.getToyList(), new File(file));

        System.out.println("\nToys import via ToysReader:");
        try {
            for (Toy toy : ToysReader.read(new File(file))) {
                System.out.println(toy);
            }
        } catch (FileNotFoundException e) {
            throw new ReadWriteException("Exception occurred while reading from " + file, e);
        }


        Collections.sort(room.getToyList(), new ToyNameComparator());
        System.out.println("\nThis is a Play Room sorted by ToyName:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        Collections.sort(room.getToyList(), new ToyAgeComparator());
        System.out.println("\nThis is Play Room sorted by ToyAge:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        Collections.sort(room.getToyList(), new ToyCostComparator());
        System.out.println("\nThis is a Play Room sorted by ToyCost:");
        for (Toy toy : room.getToyList()) {
            System.out.println(toy);
        }

        ToyFinderByName.findToysByName("Brick", room.getToyList());
        System.out.println("\nToys found by name:");
        for (Toy toy : ToyFinderByName.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by name:");
        for (Toy toy : room.find(new FindConditionByName("Brick"))) {
            System.out.println(toy);
        }

        ToyFinderByAge.findToysByAge(1, 2, room.getToyList());
        System.out.println("\nToys found by age:");
        for (Toy toy : ToyFinderByAge.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by age:");
        for (Toy toy : room.find(new FindConditionByAge(1, 2))) {
            System.out.println(toy);
        }

        ToyFinderBySize.findToysBySize(1, 2, room.getToyList());
        System.out.println("\nToys found by size:");
        for (Toy toy : ToyFinderBySize.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by size:");
        for (Toy toy : room.find(new FindConditionByToySize(1, 2))) {
            System.out.println(toy);
        }

        ToyFinderByCost.findToysByCost(1.1, 2.2, room.getToyList());
        System.out.println("\nToys found by cost:");
        for (Toy toy : ToyFinderByCost.getFoundToys()) {
            System.out.println(toy);
        }

        System.out.println("\nToys found by cost:");
        for (Toy toy : room.find(new FindConditionByCostRange(1.1, 2.2))) {
            System.out.println(toy);
        }

        System.out.println("\nToy after playing with them:");
        for (int i = 0; i < room.getToyList().size(); i++) {
            room.getToyList().get(i).play();
            System.out.println(room.getToyList().get(i).toString());
        }


    }

}
