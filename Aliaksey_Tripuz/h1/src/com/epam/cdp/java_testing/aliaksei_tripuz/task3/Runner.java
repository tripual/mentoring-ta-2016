package com.epam.cdp.java_testing.aliaksei_tripuz.task3;

import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        int toValueSumm1, toValueSumm2;
        while (true) {
            System.out.println("Please, enter number of menu:");
            System.out.println("1 - Summ");
            System.out.println("2 - Abs");
            System.out.println("3 - Mult");
            System.out.println("4 - Div");
            System.out.println("0 - Exit");
            Scanner scanner = new Scanner(System.in);
            int action = scanner.nextInt();
            if (action == 0) {

                System.out.println("Completed");
                break;
            }
            System.out.println("Please, enter A: ");
            toValueSumm1 = scanner.nextInt();
            System.out.println("Please, enter B: ");
            toValueSumm2 = scanner.nextInt();
            Calculator calc1 = new Calculator(toValueSumm1, toValueSumm2);
            switch (action) {
                case 1:
                    System.out.println("Sum = " + calc1.sum());
                    break;
                case 2:
                    System.out.println("Abs = " + calc1.abs());
                    break;
                case 3:
                    System.out.println("Mult = " + calc1.mult());
                    break;
                case 4:
                    if (toValueSumm2 == 0) {
                        System.out.println("Zero is not allowed. Please, repeat");
                        continue;
                    } else {
                        Calculator calc2 = new Calculator(toValueSumm1, toValueSumm2);
                        System.out.println("Div = " + calc2.div());
                    }
                    break;
                default:
                    System.out.println("Incorrect value! Please, try again.");
            }
        }
    }
}

