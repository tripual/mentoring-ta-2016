package com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions;

public class ValidateMaxTotalToysCostException extends Exception {

    public ValidateMaxTotalToysCostException(String message) {
        super(message);
    }
}

