package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.List;

public abstract class ToyParser {

    protected String toyName;
    protected int toyAge;
    protected double toyCost;
    protected int toySize;

    public void setToyName(String toyToken) {
        if (toyToken.startsWith("toyName")) {
            this.toyName = toyToken.substring(toyToken.lastIndexOf("=") + 1);
        }
    }

    public void setToyAge(String toyToken) {
        if (toyToken.startsWith("toyAge")) {
            this.toyAge = Integer.parseInt(toyToken.substring(toyToken.lastIndexOf("=") + 1));
        }
    }

    public void setToyCost(String toyToken) {
        if (toyToken.startsWith("toyCost")) {
            this.toyCost = Double.parseDouble(toyToken.substring(toyToken.lastIndexOf("=") + 1));
        }
    }

    public void setToySize(String toyToken) {
        if (toyToken.startsWith("toySize")) {
            this.toySize = Integer.parseInt(toyToken.substring(toyToken.lastIndexOf("=") + 1));
        }
    }

    public void setToyFields(String toyToken) {
        setToyName(toyToken);
        setToyAge(toyToken);
        setToyCost(toyToken);
        setToySize(toyToken);
    }

    public abstract Toy parseToy(List<String> toyObject);

    abstract boolean validateToy();

}
