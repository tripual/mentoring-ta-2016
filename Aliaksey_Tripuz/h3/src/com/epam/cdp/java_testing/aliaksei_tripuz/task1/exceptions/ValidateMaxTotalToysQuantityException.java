package com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions;

public class ValidateMaxTotalToysQuantityException extends Exception {

    public ValidateMaxTotalToysQuantityException(String message) {
        super(message);
    }
}
