package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

public enum ToySize {
    BIG,
    MIDDLE,
    LITTLE;
}

