package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.ArrayList;
import java.util.List;

public class ToyFinderByAge {

    private static List<Toy> foundToys = new ArrayList<>();

    public static List<Toy> getFoundToys() {
        return foundToys;
    }

    public static void findToysByAge(int RangeFrom, int RangeTo, List<Toy> toyList) {
        for (Toy toy : toyList) {
            if ((toy.getToyAge() >= RangeFrom) && (toy.getToyAge() <= RangeTo)) {
                getFoundToys().add(toy);
            }
        }
    }
}
