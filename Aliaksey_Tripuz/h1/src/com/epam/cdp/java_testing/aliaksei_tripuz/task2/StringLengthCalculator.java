package com.epam.cdp.java_testing.aliaksei_tripuz.task2;


public class StringLengthCalculator {

    String[] anArrayOfStrings;

    public StringLengthCalculator(String[] anArrayOfStrings) {
        this.anArrayOfStrings = anArrayOfStrings;
    }

    public String max() {
        int maxStringLength = 0;
        String longestString = null;
        for (String s : this.anArrayOfStrings) {
            if (s.length() > maxStringLength) {
                maxStringLength = s.length();
                longestString = s;
            }
        }
        return longestString;
    }

    public String min() {
        int minStringLength = Integer.MAX_VALUE;
        String shortestString = null;
        for (String s : this.anArrayOfStrings) {
            if (s.length() < minStringLength) {
                minStringLength = s.length();
                shortestString = s;
            }
        }
        return shortestString;
    }

}
