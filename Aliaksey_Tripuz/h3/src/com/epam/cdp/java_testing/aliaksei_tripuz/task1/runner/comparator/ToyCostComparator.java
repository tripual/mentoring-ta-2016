package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.comparator;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.Comparator;

public class ToyCostComparator implements Comparator<Toy> {

    @Override
    public int compare(Toy o1, Toy o2) {
        String name1 = String.valueOf(o1.getToyCost());
        String name2 = String.valueOf(o2.getToyCost());
        return name1.compareTo(name2);
    }

}

