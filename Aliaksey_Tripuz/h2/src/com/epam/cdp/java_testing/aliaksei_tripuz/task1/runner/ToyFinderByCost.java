package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.ArrayList;
import java.util.List;

public class ToyFinderByCost {

    private static List<Toy> foundToys = new ArrayList<>();

    public static List<Toy> getFoundToys() {
        return foundToys;
    }

    public static void findToysByCost(double RangeFrom, double RangeTo, List<Toy> toyList) {
        for (Toy toy : toyList) {
            if ((toy.getToyCost() >= RangeFrom) && toy.getToyCost() <= RangeTo) {
                getFoundToys().add(toy);
            }
        }
    }
}
