package com.epam.cdp.java_testing.aliaksei_tripuz.task1;

import java.util.Arrays;
import java.util.Random;


public class Runner {

    public static void main(String[] args) {
        int numberOfElements = 20;
        int minArrayValue = 10;
        Integer[] anArray = new Integer[numberOfElements];

        for (int arrElementNo = 0; arrElementNo < anArray.length; arrElementNo++) {
            Random ran = new Random();
            int line = (ran.nextInt(numberOfElements) - minArrayValue);
            anArray[arrElementNo] = line;
        }
        System.out.println("This is an array of integers: " + Arrays.toString(anArray));

        Calculator calculator = new Calculator(anArray);
        System.out.println(calculator.convert());

    }
}


