package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


public interface IStartEndToken {
    String START_TOKEN = "startObject";
    String END_TOKEN = "endObject";
}
