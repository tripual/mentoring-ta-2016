package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

public class Car extends Toy {

    private String wheels;

    public Car(String toyName, int toyAge, double toyCost, int toySize, String wheels) {
        super(toyName, toyAge, toyCost, toySize);
        this.wheels = wheels;
    }

    public String getWheels() {
        return wheels;
    }

    public void setWheels(String wheels) {
        this.wheels = wheels;
    }

    @Override
    public void play() {
        super.play();
        setWheels("Turning the " + getWheels());
    }

    @Override
    public String toString() {
        return super.getInfo() + ", Wheels: " + getWheels();
    }
}
