package com.epam.cdp.java_testing.aliaksei_tripuz.task2;

import java.util.Scanner;
import java.util.Arrays;

public class Runner {

    public static void main(String[] args) {
        System.out.println("Please enter the number of strings: ");
        Scanner scanner = new Scanner(System.in);
        int numberOfStrings = Integer.parseInt(scanner.nextLine());
        String[] anArray = new String[numberOfStrings];

        for (int arrElementNo = 0; arrElementNo < numberOfStrings; arrElementNo++) {
            System.out.println("Please enter string number " + (arrElementNo + 1) + ":");
            anArray[arrElementNo] = scanner.nextLine();
        }
        System.out.println("This is an array of strings: " + Arrays.toString(anArray));

        StringLengthCalculator calc1 = new StringLengthCalculator(anArray);

        String maxLength = calc1.max();
        System.out.println("Longest string is: " + maxLength);
        System.out.println("Longest string length is: " + maxLength.length());

        String minLength = calc1.min();
        System.out.println("Shortest string is: " + minLength);
        System.out.println("Shortest string length is: " + minLength.length());

    }
}

