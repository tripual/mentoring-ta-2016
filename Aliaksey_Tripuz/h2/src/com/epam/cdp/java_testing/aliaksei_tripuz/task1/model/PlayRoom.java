package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.FindCondition;

import java.util.ArrayList;
import java.util.List;

public class PlayRoom {

    private int maxTotalToysCost;
    private int maxTotalToysQuantity;

    public PlayRoom(int maxTotalToysCost, int maxTotalToysQuantity) {
        this.maxTotalToysCost = maxTotalToysCost;
        this.maxTotalToysQuantity = maxTotalToysQuantity;
    }

    private List<Toy> toyList = new ArrayList<>();

    public List<Toy> getToyList() {
        return toyList;
    }

    public void addToy(Toy toy) {
        if (validateTotalToysCost(toy) && validateTotalToysQuantity()) {
            getToyList().add(toy);
        } else if (!validateTotalToysCost(toy)) {
            System.out.println("Cannot add toy: max toys cost: " + maxTotalToysCost + ", toys cost after adding the toy would be: " + (calculateTotalToysCost() + toy.getToyCost()));
            System.out.println(toy);
        } else if (!validateTotalToysQuantity()) {
            System.out.println("Cannot add toy: max toys quantity: " + maxTotalToysQuantity + ", current toys quantity: " + (getToyList().size()));
            System.out.println(toy);
        }
    }

    private double calculateTotalToysCost() {
        double TotalToysAmount = 0;
        for (Toy toy : getToyList()) {
            TotalToysAmount = TotalToysAmount + toy.getToyCost();
        }
        return TotalToysAmount;
    }

    private boolean validateTotalToysCost(Toy toy) {
        if ((calculateTotalToysCost() + toy.getToyCost()) < maxTotalToysCost) {
            return true;
        } else if ((calculateTotalToysCost() + toy.getToyCost()) >= maxTotalToysCost) {
            return false;
        }
        return false;
    }

    private boolean validateTotalToysQuantity() {
        if (getToyList().size() < maxTotalToysQuantity) {
            return true;
        } else if (getToyList().size() >= maxTotalToysQuantity) {
            return false;
        }
        return false;
    }

    public List<Toy> find(FindCondition condition) {
        List<Toy> result = new ArrayList<>();
        for (Toy toy : getToyList()) {
            if (condition.apply(toy)) {
                result.add(toy);
            }
        }
        return result;
    }

}
