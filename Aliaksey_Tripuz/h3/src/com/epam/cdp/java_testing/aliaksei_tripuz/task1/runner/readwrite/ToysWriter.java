package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.readwrite;


import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ReadWriteException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.*;

import java.io.*;

import java.util.List;

public class ToysWriter {


    public static String writeCommonToysFields(Toy toy) {
        return String.valueOf("toyName=" + toy.getToyName()) + "\n" + "toyAge=" + toy.getToyAge() + "\n" +
                "toyCost=" + toy.getToyCost() + "\n" + "toySize=" + toy.getToySize() + "\n";
    }

    public static void writeToysToFile(List<Toy> arguments, File file) {
        try (
                BufferedWriter writer = new BufferedWriter(new FileWriter(file))
        ) {
            for (Toy toy : arguments) {
                if (toy instanceof Car) {
                    Car car = (Car) toy;
                    writer.write(String.valueOf("startObject=" + Car.class.getName()) + "\n");
                    writer.write(writeCommonToysFields(car));
                    writer.write(String.valueOf("carWheels=" + car.getWheels()) + "\n");
                    writer.write(String.valueOf("endObject=" + Car.class.getName()) + "\n");

                } else if (toy instanceof Ball) {
                    Ball ball = (Ball) toy;
                    writer.write(String.valueOf("startObject=" + Ball.class.getName()) + "\n");
                    writer.write(writeCommonToysFields(ball));
                    writer.write(String.valueOf("ballRubber=" + ball.getRubber()) + "\n");
                    writer.write(String.valueOf("endObject=" + Ball.class.getName()) + "\n");

                } else if (toy instanceof Doll) {
                    Doll doll = (Doll) toy;
                    writer.write(String.valueOf("startObject=" + Doll.class.getName()) + "\n");
                    writer.write(writeCommonToysFields(doll));
                    writer.write(String.valueOf("dollHair=" + doll.getHair()) + "\n");
                    writer.write(String.valueOf("endObject=" + Doll.class.getName()) + "\n");

                } else if (toy instanceof Brick) {
                    Brick brick = (Brick) toy;
                    writer.write(String.valueOf("startObject=" + Brick.class.getName()) + "\n");
                    writer.write(writeCommonToysFields(brick));
                    writer.write(String.valueOf("brickEdges=" + brick.getEdges()) + "\n");
                    writer.write(String.valueOf("endObject=" + Brick.class.getName()) + "\n");
                }
            }
            writer.flush();

        } catch (IOException e) {
            throw new ReadWriteException("Exception occurred while writing to " + file, e);
        }
    }
}
