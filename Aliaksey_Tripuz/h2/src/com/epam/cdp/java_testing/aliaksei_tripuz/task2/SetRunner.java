package com.epam.cdp.java_testing.aliaksei_tripuz.task2;

import java.util.*;

public class SetRunner {

    public static void main(String[] args) {
        int numberOfElements = 50000;
        int numberOfExecutionTimes = 100;

        Set<Integer> set = new HashSet<>();
        //Set<Integer> set = new TreeSet<>();

        List<Long> addExecutionTimeList = new ArrayList<>();
        List<Long> removeExecutionTimeList = new ArrayList<>();
        List<Long> findExecutionTimeList = new ArrayList<>();
        List<Long> clearExecutionTimeList = new ArrayList<>();

        for (int y = 0; y <= numberOfExecutionTimes; y++) {

            //Checking time of adding elements
            long addStartTime = System.nanoTime();
            for (int i = 0; i < numberOfElements; i++) {
                Integer a = new Random().nextInt();
                set.add(a);
            }
            long addExecutionTime = System.nanoTime() - addStartTime;
            addExecutionTimeList.add(addExecutionTime);


            //Checking time of deleting one random element
            long removeStartTime = System.nanoTime();
            set.remove(new Random().nextInt(numberOfElements));
            long removeExecutionTime = System.nanoTime() - removeStartTime;
            removeExecutionTimeList.add(removeExecutionTime);


            //Checking time of finding one random element
            long findStartTime = System.nanoTime();
            set.contains(new Random().nextInt(numberOfElements));
            long findExecutionTime = System.nanoTime() - findStartTime;
            findExecutionTimeList.add(findExecutionTime);


            //Checking time of deleting all elements
            long clearStartTime = System.nanoTime();
            set.clear();
            long clearExecutionTime = System.nanoTime() - clearStartTime;
            clearExecutionTimeList.add(clearExecutionTime);
        }

        System.out.println("Average time of adding elements in nanoseconds: " + calculateAverageTime(addExecutionTimeList));
        System.out.println("Average time of deleting one random element in nanoseconds: " + calculateAverageTime(removeExecutionTimeList));
        System.out.println("Average time of finding one random element in nanoseconds: " + calculateAverageTime(findExecutionTimeList));
        System.out.println("Average time of deleting all elements in nanoseconds: " + calculateAverageTime(clearExecutionTimeList));

    }

    public static long calculateAverageTime(List<Long> executionTimeList) {
        long sum = 0;
        for (long element : executionTimeList) {
            sum = sum + element;
        }
        return sum / executionTimeList.size();
    }

}
