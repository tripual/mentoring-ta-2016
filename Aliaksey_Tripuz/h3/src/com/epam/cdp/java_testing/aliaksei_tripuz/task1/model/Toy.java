package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

public class Toy implements IPlayable {

    protected String toyName;
    protected int toyAge;
    protected double toyCost;
    protected int toySize;

    public Toy(String toyName, int toyAge, double toyCost, int toySize) {
        this.toyName = toyName;
        this.toyAge = toyAge;
        this.toyCost = toyCost;
        this.toySize = toySize;
    }

    public Toy() {
    }

    public String getToyName() {
        return toyName;
    }

    public void setToyName(String toyName) {
        this.toyName = toyName;
    }

    public int getToyAge() {
        return toyAge;
    }

    public void setToyAge(int toyAge) {
        this.toyAge = toyAge;
    }

    public double getToyCost() {
        return toyCost;
    }

    public void setToyCost(double toyCost) {
        this.toyCost = toyCost;
    }

    public int getToySize() {
        return toySize;
    }

    public void setToySize(int toySize) {
        this.toySize = toySize;
    }

    public void play() {
        setToyName("Super " + getToyName());
    }

    public String getInfo() {
        return "Toy Name: " + getToyName() + ", Toy Age: " + getToyAge() + ", Toy Cost: " + getToyCost() + ", Toy Size: " + getToySize();
    }

}
