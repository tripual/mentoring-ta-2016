package com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.model.Toy;

import java.util.Arrays;
import java.util.List;

public class FindConditionByName implements FindCondition {

    List<String> names;

    public FindConditionByName(String... names) {
        this.names = Arrays.asList(names);
    }

    public boolean apply(Toy toy) {
        return names.contains(toy.getToyName());
    }
}
