package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateMaxTotalToysCostException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.exceptions.ValidateMaxTotalToysQuantityException;
import com.epam.cdp.java_testing.aliaksei_tripuz.task1.runner.findcondition.FindCondition;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PlayRoom {

    private double maxTotalToysCost;
    private int maxTotalToysQuantity;


    public PlayRoom(double maxTotalToysCost, int maxTotalToysQuantity) throws ValidateMaxTotalToysCostException, ValidateMaxTotalToysQuantityException {
        this.maxTotalToysCost = maxTotalToysCost;
        this.maxTotalToysQuantity = maxTotalToysQuantity;

        if (maxTotalToysCost <= 0) {
            throw new ValidateMaxTotalToysCostException("maxTotalToysCost must be > 0");
        } else if (maxTotalToysQuantity <= 0) {
            throw new ValidateMaxTotalToysQuantityException("maxTotalToysQuantity must be > 0");
        }

    }


    private List<Toy> toyList = new ArrayList<>();

    public List<Toy> getToyList() {
        return toyList;
    }

    private List<String> notAddedToys = new ArrayList<>();

    public List<String> getNotAddedToys() {
        return notAddedToys;
    }

    public void addToy(Toy toy) {
        if (validate(toy)) {
            getToyList().add(toy);
        } else {
            getNotAddedToys().add("Toy not added: max cost: " + maxTotalToysCost + ", cost to be after adding toy: " +
                    Double.parseDouble(new DecimalFormat("##.##").format((calculateTotalToysCost() + toy.getToyCost()))) +
                    ", max toys quantity: " + maxTotalToysQuantity + ", current toys quantity: " + (getToyList().size()) + "\n" + toy);
        }
    }


    private double calculateTotalToysCost() {
        double totalToysCost = 0;
        for (Toy toy : getToyList()) {
            totalToysCost = totalToysCost + toy.getToyCost();
        }
        return totalToysCost;
    }

    private boolean validate(Toy toy) {
        return validateCost(toy) && validateQuantity();
    }

    private boolean validateQuantity() {
        return getToyList().size() < maxTotalToysQuantity;
    }

    private boolean validateCost(Toy toy) {
        return (calculateTotalToysCost() + toy.getToyCost()) < maxTotalToysCost;
    }

    public List<Toy> find(FindCondition condition) {
        List<Toy> result = new ArrayList<>();
        for (Toy toy : getToyList()) {
            if (condition.apply(toy)) {
                result.add(toy);
            }
        }
        return result;
    }

}
