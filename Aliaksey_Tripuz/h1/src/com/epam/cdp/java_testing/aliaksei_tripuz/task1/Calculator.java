package com.epam.cdp.java_testing.aliaksei_tripuz.task1;


import java.util.Arrays;

public class Calculator {

    private Integer[] anArray;

    public Calculator(Integer[] anArray) {
        this.anArray = anArray;
    }


    public String convert() {
        int minPositiveElement = 10;
        int maxNegativeElement = -10;
        for (int s : anArray) {
            if (s < minPositiveElement && s > 0) {
                minPositiveElement = s;

            } else if (s > maxNegativeElement && s < 0) {
                maxNegativeElement = s;
            }

        }

        int MinPositiveElementIndex = Arrays.asList(anArray).indexOf(minPositiveElement);
        int MaxNegativeElementIndex = Arrays.asList(anArray).indexOf(maxNegativeElement);

        anArray[MinPositiveElementIndex] = maxNegativeElement;
        anArray[MaxNegativeElementIndex] = minPositiveElement;

        return "Min positive element: " + minPositiveElement + "\nIndex: " + MinPositiveElementIndex + "\nMax negative element: " + maxNegativeElement + "\nIndex: " + MaxNegativeElementIndex + "\nThis is an updated array of integers: " + Arrays.toString(anArray);
    }


}
