package com.epam.cdp.java_testing.aliaksei_tripuz.task1.model;

public class Store {

    public static Car buyCar(int toySize) {
        if (toySize == ToySizes.LITTLE_SIZE) {
            return new Car("Car", 1, 2.4, toySize, "little wheels");
        } else if (toySize == ToySizes.MIDDLE_SIZE) {
            return new Car("Car", 4, 3.3, toySize, "big wheels");
        } else if (toySize == ToySizes.BIG_SIZE) {
            return new Car("Car", 6, 5.2, toySize, "big wheels");
        }
        return Store.buyCar(0);
    }

    public static Ball buyBall(int toySize) {
        if (toySize == ToySizes.LITTLE_SIZE) {
            return new Ball("Ball", 2, 1.3, toySize, "thick rubber");
        } else if (toySize == ToySizes.MIDDLE_SIZE) {
            return new Ball("Ball", 5, 2.1, toySize, "thin rubber");
        } else if (toySize == ToySizes.BIG_SIZE) {
            return new Ball("Ball", 7, 5.4, toySize, "thin rubber");
        }
        return Store.buyBall(0);
    }

    public static Doll buyDoll(int toySize) {
        if (toySize == ToySizes.LITTLE_SIZE) {
            return new Doll("Doll", 3, 5.2, toySize, "curly hair");
        } else if (toySize == ToySizes.MIDDLE_SIZE) {
            return new Doll("Doll", 5, 6.1, toySize, "straight hair");
        } else if (toySize == ToySizes.BIG_SIZE) {
            return new Doll("Doll", 8, 8.6, toySize, "wavy hair");
        }
        return Store.buyDoll(0);
    }

    public static Brick buyBrick(int toySize) {
        if (toySize == ToySizes.LITTLE_SIZE) {
            return new Brick("Brick", 0, 1.6, toySize, "little edges");
        } else if (toySize == ToySizes.MIDDLE_SIZE) {
            return new Brick("Brick", 2, 2.5, toySize, "middle size edges");
        } else if (toySize == ToySizes.BIG_SIZE) {
            return new Brick("Brick", 3, 3.1, toySize, "big size edges");
        }
        return Store.buyBrick(0);
    }

}
